<?php
declare(strict_types=1);

/**
 * This file is included by composer, which means it will be included when the phinx command
 * loads.
 *
 * If it detects that we're running the phinx command, it will chdir to __DIR__ because
 * phinx expects to find the config file in that folder.
 *
 * Next, phinx will load phinx.php to receive the configuration information - and this script
 * will return the config array.
 */
global $argv;

if (!isset($argv[0]) || 'phinx' !== basename($argv[0])) {
    return;
}

if (!empty($GLOBALS['phinxified'])) {
    chdir($GLOBALS['phinxified']);
    unset($GLOBALS['phinxified']);
    $root = Charm\Util\ComposerPath::get();

    // Final paths where phinx will run migrations from
    $migrationPaths = [];

    // Used to identify duplicated migration folders by migration file names
    $addedMigrationFiles = [];

    echo "Searching for migrations: \r";
    $queue = [ $root, $root.'/vendors/charm' ];
    while (null !== ($next = array_shift($queue))) {
        if (!is_dir($next)) {
            // queued dirs might not exist
            continue;
        }
        $candidates = glob($next.'/*', \GLOB_ONLYDIR);
        foreach ($candidates as $candidate) {
            echo "Searching for migrations in '$candidate'                      \r";
            if (basename($candidate) === 'migrations') {
                $allFiles = glob($candidate.'/*');
                if (count($allFiles) === 0) {
                    // Empty dirs can safely be added to config, and are likely a migrations folder
                    $migrationPaths[] = $candidate;
                    echo " - found '$candidate'                                         \n";
                } else {
                    $migrationFiles = glob($candidate.'/*.php');
                    $matchedFiles = [];
                    $failedFiles = [];
                    foreach ($migrationFiles as $migrationFile) {
                        $bn = basename($migrationFile);
                        if (array_key_exists($bn, $addedMigrationFiles)) {
                            throw new Error("Found duplicate migration files '$migrationFile' and '".$addedMigrationFiles[$bn]."'");
                        }

                        $c = preg_match('/^[0-9]{14}_[a-z0-9_]+\.php$/', $bn, $matches);
                        if (isset($matches[0])) {
                            $matchedFiles[] = $migrationFile;
                        } else {
                            $failedFiles[] = $migrationFile;
                        }
                    }
                    if (count($failedFiles) > 0) {
                        throw new Error("Invalid migration file names found: ".implode(", ", array_keys($failedFiles))." in '$candidate'");
                    }

                    foreach ($matchedFiles as $matchedFile) {
                        $addedMigrationFiles[basename($matchedFile)] = $matchedFile;
                    }
                    $migrationPaths[] = $candidate;
                    echo " - found '$candidate'                                         \n";
                }
            }
            if (basename($candidate) === 'vendor') {
                // skip vendor dirs not explicitly added
                continue;
            }
            $queue[] = $candidate;
        }
    }

    if (sizeof($migrationPaths) === 0) {
        echo "No folders named 'migrations' found. Before you can use `charm/phinx` you must create a folder named 'migrations' somewhere.\n";
        die();
    }

    $seedPaths = [];

    $dbConfig = Charm\DB::getConfig();

    $dsn = $dbConfig['dsn'];

    $adapter = substr($dsn, 0, strpos($dsn, ':'));
    $dsn = substr($dsn, strpos($dsn, ':') + 1);
    $dsnParts = explode(';', $dsn);

    $getPart = function ($name) use ($dsnParts) {
        foreach ($dsnParts as $d) {
            if (0 === strpos($d, $name)) {
                return substr($d, strpos($d, '=') + 1);
            }
        }

        return null;
    };

    switch ($adapter) {
        case 'mysql':
            $e['adapter'] = 'mysql';
            $e['host'] = $getPart('host');
            $e['name'] = $getPart('dbname');
            $e['user'] = $dbConfig['username'];
            $e['pass'] = $dbConfig['password'];
            $e['port'] = $getPart('port') ?? '3306';
            $e['charset'] = $getPart('charset') ?? 'utf8mb4';
            $e['collation'] = $getPart('collation') ?? 'utf8mb4_unicode_ci';
            break;
        case 'sqlite':
            $e['adapter'] = 'sqlite';
            if (':memory' == $dsn) {
                $e['memory'] = true;
            } else {
                $e['name'] = $dsn;
            }
            break;
        default:
            throw new Exception('`charm/phinx` does not support the adapter '.$adapter);
    }

    return [
        'paths' => [
            'migrations' => $migrationPaths,
            'seeds' => $seedPaths,
        ],
        'environments' => [
            'default_migration_table' => 'migrations_log',
            'default_database' => 'current',
            'current' => $e,
        ],
    ];
}

$GLOBALS['phinxified'] = getcwd();
chdir(__DIR__);

return;
